// ---------------------- Header ---------------------- 

$("[data-toggle=popover]").popover(
	{
		trigger: 'click',
	    html: true, 
	    placement: 'bottom',
		content: function() 
		{
	          return $('#kl_user_space_pop').remove().html()
	    }
	}
);

// ---------------------- END Header ---------------------- 

// ---------------------- Slider Home ---------------------- 
 
$('.Kl_slider_home').owlCarousel(
	{
		animateOut: 'fadeOutRight',
		animateIn: 'fadeInRight',
	    items:1,
	    lazyLoad:true,
	    responsiveClass:true,
	    loop:true,
	    margin:10
	}
);	

// ---------------------- END Slider Home ---------------------- 